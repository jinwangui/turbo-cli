> **Node 版本需要大于 14.14.0**
# pang-Cli

生成项目模版用

## 使用

### 全局安装

```
npm install -g pang-cli
```
命令

```
pang init
```

### 局部安装

```
npm install pang-cli
```
命令
```
npx pang init
```

## 特点
- turbo 用于turbo子项目创建


