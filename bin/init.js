#!/usr/bin/env node
const { Command } = require('commander');
const inquirer = require('inquirer');
const generator = require('./generator');
const program = new Command();

program.parse(process.argv);


const questions = [
  {
    type: 'list',
    name: 'type',
    message: 'Please select templete type',
    choices: [
      'egg',
      'react',
      'turbo',
     ],
     default() {
      return 'turbo';
    },
  },
   {
     type: 'input',
     name: 'name',
     message: "Please select templete name",
     default() {
       return 'home';
     },
     validate(value) {
      if (value) {
        return true;
      }

      return 'Please input templete name';
    },
   },
   {
     type: 'input',
     name: 'port',
     message: "Please select templete port",
     validate: function(val) {
      if(val.match(/\d/g)) { // 校验位数
          return true;
      }
      return "请输入数字";
      }
   },
 ];
 
 inquirer.prompt(questions).then((answers) => {
   const { type,name, port} = answers
   generator(type,name,port)
 });
