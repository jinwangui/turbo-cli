import { SITE_MAP_URL } from '@constants/paths';
import { withXMLResponse, formatContent } from '@utils/sitemap';

export const getServerSideProps = async (ctx) => {
  let sitemapEN = ''
  await fetch(`${SITE_MAP_URL}/sitemap-0.xml`)
    .then(res => res.text())
    .then(response => {
      sitemapEN = response
    })
    .catch(err => {
      console.log(err);
    })

  const body = await formatContent(sitemapEN, 'zh-CN')
  return withXMLResponse(ctx, body)
}

// Default export to prevent next.js errors
export default function Sitemap () {}
